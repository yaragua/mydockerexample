# https://github.com/docker/docker-birthday-3/blob/master/tutorial.md
# our base image
FROM alpine:latest

# Install python and pip
RUN apk add --update py-pip

# install Python modules needed by the Python app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

# Actualizando el PIP
RUN pip install --upgrade pip

# copy files required for the app to run
COPY app.py /usr/src/app/
COPY templates/index.html /usr/src/app/templates/

# tell the port number the container should expose
EXPOSE 5000

# run the application
CMD ["python", "/usr/src/app/app.py"]

# Comandos a ejecutar por terminal

# Ejecutar este cada vez que queramos compilar desde cero
# docker build -t luiszambrano/myfirstapp .
# #########################################

# Arrancamos la maquina
# docker run -p 8881:5000 --name myfirstapp luiszambrano/myfirstapp

# buscamos la ip general, y ubicamos el puerto con el segundo comando.
# docker-machine ip default
# docker port myfirstapp
